##### Installation (Debian/Ubuntu)

```bash
curl -sL https://raw.githubusercontent.com/tna76874/podjava/main/install.sh | bash
```

## podjava

containerized java

```bash
podjava java -version
```

## podpod

Containerized jupyter with java

##### Installation (Debian/Ubuntu)

Startup

```bash
podpod
```

and open http://localhost:8888/
